const repo = "/opt/percy";
const port = process.env.PORT || 8888;
let percy_token = '8bb79f6141bc0d56ff226bf7f9f3bfc5be36e822855b3387697bc2f90f60999c';
let url = 'https://qa.nar.realtor/';
let isRunnig = false;


let http = require('http');
//const exec = require('child_process').exec;
const { spawn } = require('child_process');

const path = require('path');
const qs = require('querystring')
const fs = require('fs')

var server = http.createServer(handler).listen(port, function(err){
    if(err){
        console.log('Error starting http server');
    } else {
        console.log('Server listening on port ' + port);
    };
});

server.on('listening',function(){
    console.log('ok, server is running');
});

// Create a function to handle every HTTP request
function handler(req, res){
    if (req.method == "GET"){ 
      const { url } = req;
      console.log('get');
        if (url == "/css/style.css") {
            fs.readFile(__dirname + '/public/css/style.css', function (err, data) {
                if (err) return send404(res);
                res.writeHead(200, { 'Content-Type': 'text/css' });
                res.end(data, 'utf-8');
                res.end();
              });
        } 
        else {
            res.setHeader('Content-Type', 'text/html');
            res.writeHead(200);
            res.end(`<html>
                    <head>
                    <link rel="stylesheet" href="/css/style.css" type="text/css" />
                    </head>
                        <body>
                            <form action="/" method="post">
                            <input class="button" type="submit" name="submit_test" value="Take snapshots from TEST">
                            <input class="button" type="submit" name="submit_qa" value="Take snapshots from QA">
                            <input class="button" type="submit" name="submit_prod" value="Take snapshots from PROD">
                            </form>
                        </body>
                    </html>`);
        }
    } else if(req.method == 'POST'){
        console.log('post');
        var body = '';
        req.on('data', function (data) {
            body += data;
            if (body.length > 1e6)
                req.connection.destroy();
        });

        req.on('end', function () {
            var postData = qs.parse(body);
            // use post['blah'], etc.

            if ('submit_test' in postData ) {
                percy_token = '647b2511219381d895d38dcab9633e74f673440d68510f4a38078f040b839256';
                url = 'https://test.nar.realtor/'
            }
            if ('submit_qa' in postData ) {
                percy_token = '8bb79f6141bc0d56ff226bf7f9f3bfc5be36e822855b3387697bc2f90f60999c';
                url = 'https://qa.nar.realtor/'
            }
            if ('submit_prod' in postData ) {
                percy_token = '5f166884155eea53c0162ef7762daa98398d689725530b1f4cd4243d78910194';
                url = 'https://nar.realtor/'
            }
            
            //console.log('cd ' + repo + ' && export PERCY_TOKEN=' + percy_token + ' && npm run snapshots ' + url);
            if (!isRunnig) {
                isRunnig = true;
                
                const child = spawn('npx',['percy', 'exec', '--', 'node', 'snapshots.js'], {
                    env: {
                        ...process.env,
                        PERCY_TOKEN: percy_token,
                        URL: url
                    },
                });
                child.on('exit', code => {
                    console.log(`Exit code is: ${code}`);
                    isRunnig = false;
                  });
            }
        });
  
      // Here you could use a library to extract the form content
      // The Express.js web framework helpfully does just that
      // For simplicity's sake we will always respond with 'hello world' here
      //console.log(req);

      res.setHeader('Content-Type', 'text/html');
      res.writeHead(200);
      res.end(`<html>
        <head>
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        </head>
            <body>
                <h2>${isRunnig ? "Runnig process exists, try a bit later..." : "Process is started successfully"}</h2>
                <form action="/" method="post">
                    <input class="button" type="submit" name="submit_test" value="Take snapshots from TEST">
                    <input class="button" type="submit" name="submit_qa" value="Take snapshots from QA">
                    <input class="button" type="submit" name="submit_prod" value="Take snapshots from PROD">
                </form>
            </body>
        </html>`);
    } else {
      res.writeHead(200);
      res.end();
    };
  }; 