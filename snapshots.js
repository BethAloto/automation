const PercyScript = require('@percy/script');
const exec = require('child_process').exec;
const URL = process.env.URL || 'https://qasys.nar.realtor/';

PercyScript.run(async (page, percySnapshot) => {

  let options = {
    waitUntil: 'load',
    // Remove the timeout
    timeout: 0
  };

  // Homepage
  await page.goto(URL, {"waitUntil" : "networkidle0"});
  await percySnapshot('HomePage', options);
  console.log("homepage");
/*
  // Research and Statistics
  await page.goto(URL + "research-and-statistics", options);
  await percySnapshot('Research and Statistics');

  // Topics
  await page.goto(URL + "topics", options);
  await percySnapshot('Topics');
  console.log("topics");

  // Topic
  await page.goto(URL + "safety", options);
  await percySnapshot('Topic - Safety');

  // Videos
  await page.goto(URL + "videos", options);
  await percySnapshot('Videos Landing');

  // Video
  await page.goto(URL + "videos/how-to-complete-the-ppp-loan-forgiveness-application-form-3508ez", options);
  await percySnapshot('Video');

  // Latest
  await page.goto(URL + "share", options);
  await percySnapshot('Latest/Share');
  
  // Post
  await page.goto(URL + "coronavirus-a-guide-for-realtors", options);
  await percySnapshot('Post');

  // Campaign Assets
  await page.goto(URL + "thats-who-we-r/campaign-assets-for-social-media", options);
  await percySnapshot('Campaign Assets');
*/
/*
  // Pattern Library
  await page.goto(URL + "pattern-library/styles-and-paragraphs", options);
  await percySnapshot('Pattern Library');
*/
});

