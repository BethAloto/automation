const PORT = 8008;
const repo = "/opt/percy";
const wookUUID = "69443217-9a11-400c-9831-043e420be2ca";

let http = require('http');
let crypto = require('crypto');
const exec = require('child_process').exec;


http.createServer(function (req, res) {
    req.on('data', function(chunk) {
        if (req.headers['x-hook-uuid'] == wookUUID) {
            exec('cd ' + repo + ' && git pull');
        }
    });

    res.end();
}).listen(PORT, function() {
    console.log('Ready to go!');
});